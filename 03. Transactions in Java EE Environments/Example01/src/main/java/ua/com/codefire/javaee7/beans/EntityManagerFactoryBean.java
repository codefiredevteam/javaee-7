/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javaee7.beans;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
@Singleton
public class EntityManagerFactoryBean {

    private EntityManagerFactory factory;

    public EntityManagerFactory getFactory() {
        return factory;
    }
    
    @PostConstruct
    private void init() {
        this.factory = Persistence.createEntityManagerFactory("MainPU");
    }
}
