/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javaee7.servlets;

import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ua.com.codefire.javaee7.beans.EntityManagerFactoryBean;
import ua.com.codefire.javaee7.models.Role;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
@WebServlet("/index")
public class MainServlet extends HttpServlet {
    
    @EJB
    private EntityManagerFactoryBean entityManagerFactoryBean;

    @Override
    public void init() throws ServletException {
//        EntityManagerFactory factory = Persistence.createEntityManagerFactory("MainPU");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EntityManagerFactory factory = entityManagerFactoryBean.getFactory();
        
        EntityManager manager = factory.createEntityManager();
        List<Role> roleList = manager.createQuery("SELECT u FROM Role u", Role.class).getResultList();
        manager.close();
        
        req.setAttribute("roles", roleList);
        req.getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(req, resp);
    }
}
