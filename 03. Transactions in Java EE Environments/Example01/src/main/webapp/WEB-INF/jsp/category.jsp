<%-- 
    Document   : index
    Created on : Aug 18, 2016, 2:09:40 PM
    Author     : CodeFireUA <edu@codefire.com.ua>
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Users</h1>

        <ul>
            <c:forEach items="${categories}" var="cat">
                <li>
                    <div>${cat.name}</div>
                    <ul>
                        <c:forEach items="${cat.articles}" var="art">
                            <li>
                                <b>${art.title}</b><br/>
                                <fmt:formatDate value="${art.timestamp}" />
                            </li>
                        </c:forEach>
                    </ul>
                </li>
            </c:forEach>
        </ul>
    </body>
</html>
