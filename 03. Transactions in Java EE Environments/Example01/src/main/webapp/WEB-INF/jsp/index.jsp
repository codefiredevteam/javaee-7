<%-- 
    Document   : index
    Created on : Aug 18, 2016, 2:09:40 PM
    Author     : CodeFireUA <edu@codefire.com.ua>
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Users</h1>

        <ul>
            <c:forEach items="${roles}" var="role">
                <li>
                    <div>${role.name}</div>
                    <ul>
                        <c:forEach items="${role.userList}" var="user">
                            <li>${user.username}</li>
                        </c:forEach>
                    </ul>
                </li>
            </c:forEach>
        </ul>
    </body>
</html>
