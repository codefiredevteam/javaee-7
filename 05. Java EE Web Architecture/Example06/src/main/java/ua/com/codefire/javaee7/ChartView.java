/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javaee7;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
@ApplicationScoped
@ManagedBean
public class ChartView implements Serializable {

    private LineChartModel chartModel;

    public LineChartModel getChartModel() {
        return chartModel;
    }
    
    @PostConstruct
    private void init() {
        updateData();
    }

    public void updateData() {
        chartModel = new LineChartModel();
        
        LineChartSeries chartSeries = new LineChartSeries("ABC");
        
        for (int i = 0; i < 25; i++) {
            chartSeries.set(Math.random()*10, Math.random()*10 - 5);
        }
        
        chartModel.addSeries(chartSeries);
    }
    
    public void updateButtonHandler(ActionEvent ae) {
        updateData();
    }

}
