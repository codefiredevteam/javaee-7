/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javaee7;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import ua.com.codefire.javaee7.model.Beaner;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
@ApplicationScoped
@ManagedBean(eager = true)
public class ChatHistory implements Serializable {
    
    private List<Beaner> messageHistory;

    @PostConstruct
    private void init() {
        this.messageHistory = new ArrayList<>();
    }

    public List<Beaner> getMessageHistory() {
        return messageHistory;
    }
    
}
