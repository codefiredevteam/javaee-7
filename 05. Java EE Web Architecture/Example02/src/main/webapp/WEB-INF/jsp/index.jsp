<%-- 
    Document   : index
    Created on : Aug 18, 2016, 2:09:40 PM
    Author     : CodeFireUA <edu@codefire.com.ua>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Java EE 7 Web</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <h1>Identification</h1>
            <h2>Hello ${username}!</h2>
            <form method="post">
                <div>
                    <label>Enter your name:</label><br />
                    <input name="username" value="${username}" />
                </div>
                <div>
                    <button type="submit">OK</button>
                </div>
            </form>
        </div>
    </body>
</html>