<%-- 
    Document   : index
    Created on : Aug 18, 2016, 2:09:40 PM
    Author     : CodeFireUA <edu@codefire.com.ua>
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.nio.file.Files" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java EE 7 Web</title>

        <style type="text/css">
            table {
                border-collapse: collapse;
            }

            table, th, td {
                border: 1px solid #999;
            }
            table {
                border-radius: 5px;
            }

            thead, tfoot {
                background-color: #eee;
            }

            th, td {
                padding: 3px 15px;
            }

            tbody td[colspan] {
                background-color: #f9f9f9;
            }

            tbody tr {
                background-color: #fff;

                transition: background-color 0.3s;
            }

            tbody tr:hover {
                background-color: #ddd;

                transition: background-color 0.1s;
            }
        </style>
    </head>
    <body>
        <br />
        <div>
            <table style="width: 90%; margin: auto; bo">
                <thead>
                    <tr>
                        <th colspan="3" align="center"><h3>STORE</h3></th>
                    </tr>
                    <tr>
                        <th style="width: 1%;"></th>
                        <th>Name</th>
                        <th style="width: 1%;">bytes</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="spath" items="${pathList}">
                        <tr>
                            <td>${spath.file ? 'F' : 'D'}</td>
                            <td>
                                ${spath.path.fileName}
                            </td>
                            <td>${Files.size(spath.path)}</td>
                        </tr>
                    </c:forEach>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" align="right">
                            <b>Total files: ${not empty pathList ? pathList.size() : 0}</b>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </body>
</html>
