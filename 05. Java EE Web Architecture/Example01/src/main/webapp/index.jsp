<%-- 
    Document   : index
    Created on : Aug 18, 2016, 2:09:40 PM
    Author     : CodeFireUA <edu@codefire.com.ua>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Java EE 7 Web</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <h1>SERVLET's</h1>
            <ul>
                <li>
                    <a href="/servlet/annotation">Annotated Servlet</a>
                </li>
                <li>
                    <a href="/servlet/xml">Servlet defined in XML</a>
                </li>
            </ul>
        </div>
    </body>
</html>