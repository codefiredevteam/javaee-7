/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javaee7;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
@WebServlet("/index")
public class CounterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int counter = 0;

        Cookie[] cookies = req.getCookies();

        if (cookies != null) {
            for (Cookie cook : cookies) {
                if ("ccounter".equals(cook.getName())) {
                    try {
                        counter = Integer.parseInt(cook.getValue());
                        break;
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                        break;
                    }
                }
            }
        }

        Cookie cookie = new Cookie("ccounter", Integer.toString(counter + 1));
        resp.addCookie(cookie);

        req.getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("reset") != null) {
            Cookie counter = null;
            Cookie[] cookies = req.getCookies();
            if (cookies != null) {
                for (Cookie cook : cookies) {
                    if ("ccounter".equals(cook.getName())) {
                        counter = cook;
                        break;
                    }
                }
            }
            
            if (counter != null) {
                // REMOVE COOKIE BY EXPIRATION
                counter.setMaxAge(0);
                resp.addCookie(counter);
            }
            
            req.getSession().invalidate();
        }
        
        resp.sendRedirect("/");
    }

}
