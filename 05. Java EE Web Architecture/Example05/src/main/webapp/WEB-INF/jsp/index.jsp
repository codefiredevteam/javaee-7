<%-- 
    Document   : index
    Created on : Aug 18, 2016, 2:09:40 PM
    Author     : CodeFireUA <edu@codefire.com.ua>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>

        <div>
            Count: ${counter}
            <form method="post"><input type="submit" name="reset" value="Reset" /></form>
        </div>
    </body>
</html>
