/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions.stateless;

import java.util.Map;
import javax.ejb.Local;
import ua.com.codefire.beans.entity.Good;

/**
 *
 * @author codefire
 */
@Local
public interface BucketSessionBeanLocal {

    public Map<Good, Integer> getAllGoods();

    public Good getLastAddedGood();

    public int getQuantityOfGoodsInBucket();

    public double getTotalPriceOfAllProducts();

    public void addGoodIntoBucket(Good good);

    public int removeGoodFromBucket(Good good);
    
}
