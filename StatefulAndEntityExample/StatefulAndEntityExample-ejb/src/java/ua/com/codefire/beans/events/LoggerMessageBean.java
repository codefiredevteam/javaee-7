/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.events;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import ua.com.codefire.beans.sessions.stateless.LogSaverBeanLocal;

/**
 *
 * @author CodeFire
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "myLogger"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class LoggerMessageBean implements MessageListener {

    @Inject
    private LogSaverBeanLocal logSaverBean;

    public LoggerMessageBean() {
    }

    @Override
    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        try {
            logSaverBean.saveLogIntoDatabase(textMessage.getText());
        } catch (JMSException ex) {
            Logger.getLogger(LoggerMessageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
