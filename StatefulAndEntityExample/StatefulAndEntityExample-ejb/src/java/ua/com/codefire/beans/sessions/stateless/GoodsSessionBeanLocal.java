/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions.stateless;

import java.util.List;
import javax.ejb.Local;
import ua.com.codefire.beans.entity.Good;

/**
 *
 * @author codefire
 */
@Local
public interface GoodsSessionBeanLocal {

    public List<Good> getAll();

    public Good get(int id);
    
}
