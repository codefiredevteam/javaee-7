/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions.stateful;

import java.util.Map;
import javax.ejb.Local;
import ua.com.codefire.beans.entity.Good;

/**
 *
 * @author codefire
 */
@Local
public interface GoodsHolderBeanLocal {

    public Map<Good, Integer> getAll();

    public void add(Good good);

    public int remove(Good good);

    public void clean();

    public int getQuantity();

    public double getTotal();

    public Good getLast();

}
