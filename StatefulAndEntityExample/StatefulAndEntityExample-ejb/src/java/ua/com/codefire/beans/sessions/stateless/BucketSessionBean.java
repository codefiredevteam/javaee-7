/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions.stateless;

import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import ua.com.codefire.beans.entity.Good;
import ua.com.codefire.beans.sessions.stateful.GoodsHolderBeanLocal;

/**
 *
 * @author codefire
 */
@Stateless
public class BucketSessionBean implements BucketSessionBeanLocal {

    @Inject
    private GoodsHolderBeanLocal bucketBean;

    @Override
    public Map<Good, Integer> getAllGoods() {
        return bucketBean.getAll();
    }

    @Override
    public void addGoodIntoBucket(Good good) {
        bucketBean.add(good);
    }

    @Override
    public Good getLastAddedGood() {
        return bucketBean.getLast();
    }

    @Override
    public int removeGoodFromBucket(Good good) {
        return bucketBean.remove(good);
    }

    @Override
    public int getQuantityOfGoodsInBucket() {
        return bucketBean.getQuantity();
    }

    @Override
    public double getTotalPriceOfAllProducts() {
        return bucketBean.getTotal();
    }
}
