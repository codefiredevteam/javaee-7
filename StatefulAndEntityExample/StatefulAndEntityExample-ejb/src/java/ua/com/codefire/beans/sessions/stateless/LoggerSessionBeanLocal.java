/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions.stateless;

import javax.ejb.Local;
import ua.com.codefire.beans.entity.Good;

/**
 *
 * @author CodeFire
 */
@Local
public interface LoggerSessionBeanLocal {

    public void writeLog(Good good, String sessionId);
    
}
