/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions.stateful;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import ua.com.codefire.beans.entity.Good;

/**
 *
 * @author codefire
 */
@Stateful
@SessionScoped
public class GoodsHolderBean implements GoodsHolderBeanLocal {

    private final Map<Good, Integer> bucket = new HashMap<>();
    private Good last;
    private int quantity;
    private double total;

    @Override
    public Map<Good, Integer> getAll() {
        return bucket;
    }

    @Override
    public void add(Good good) {
        if (bucket.containsKey(good)) {
            int count = bucket.get(good);
            bucket.put(good, ++count);
        } else {
            bucket.put(good, 1);
        }
        this.last = good;
        calcQuantity();
    }

    @Override
    public int remove(Good good) {
        try {
            return bucket.remove(good);
        } finally {
            calcQuantity();
        }
    }

    @Override
    public Good getLast() {
        return last;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public double getTotal() {
        return total;
    }

    @Override
    public void clean() {
        bucket.clear();
        quantity = 0;
        total = 0;
    }

    private void calcQuantity() {
        int result = 0;
        for (Map.Entry<Good, Integer> entry : bucket.entrySet()) {
            int value = entry.getValue();
            result += value;
        }
        quantity = result;
        calcTotal();
    }

    private void calcTotal() {
        double result = 0;
        for (Map.Entry<Good, Integer> good : bucket.entrySet()) {
            double price = good.getKey().getPrice();
            double value = good.getValue();
            result += price * value;
        }
        total = scaled(result);
    }

    private double scaled(double value) {
        BigDecimal decimal = new BigDecimal(value);
        return decimal.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
}
