/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions.stateless;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;
import ua.com.codefire.beans.entity.Good;

/**
 *
 * @author CodeFire
 */
@Stateless
public class LoggerSessionBean implements LoggerSessionBeanLocal {

    @Resource(mappedName = "myLogger")
    private Queue myLogger;

    @Inject
    @JMSConnectionFactory("java:comp/DefaultJMSConnectionFactory")
    private JMSContext context;

    @Override
    public void writeLog(Good good, String sessionId) {
        String log = String.format("User {%s} added: %s", sessionId, good.getName());
        // send JMS message (async)
        sendJMSMessageToMyLogger(log);
    }

    private void sendJMSMessageToMyLogger(String messageData) {
        context.createProducer().send(myLogger, messageData);
    }
    
    
}
