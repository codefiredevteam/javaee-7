/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions.stateless;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

/**
 *
 * @author CodeFire
 */
@Stateless
public class LogSaverBean implements LogSaverBeanLocal {

    @Override
    public void saveLogIntoDatabase(String log) {
        try {
            // simulate database server latency
            Thread.sleep(new Random().nextInt(5000));
        } catch (InterruptedException ex) {
            Logger.getLogger(LoggerSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("INSERT INTO DATABASE: " + log);
    }
}
