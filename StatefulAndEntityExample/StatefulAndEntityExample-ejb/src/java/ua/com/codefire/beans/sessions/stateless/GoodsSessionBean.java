/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.beans.sessions.stateless;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ua.com.codefire.beans.entity.Good;

/**
 *
 * @author codefire
 */
@Stateless
public class GoodsSessionBean implements GoodsSessionBeanLocal {

    @PersistenceContext(name = "StatefulAndEntityPU")
    private EntityManager manager;

    @Override
    public List<Good> getAll() {
        return manager.createNamedQuery("Good.findAll", Good.class).getResultList();
    }

    @Override
    public Good get(int id) {
        return manager.find(Good.class, id);
    }

}
