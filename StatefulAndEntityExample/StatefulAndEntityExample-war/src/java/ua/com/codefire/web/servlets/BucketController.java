/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.web.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ua.com.codefire.beans.entity.Good;
import ua.com.codefire.beans.sessions.stateless.BucketSessionBeanLocal;
import ua.com.codefire.beans.sessions.stateless.GoodsSessionBeanLocal;
import ua.com.codefire.beans.sessions.stateless.LoggerSessionBeanLocal;

/**
 *
 * @author codefire
 */
@WebServlet("/bucket")
public class BucketController extends HttpServlet {

    @EJB
    private GoodsSessionBeanLocal goodsBean;

    @EJB
    private BucketSessionBeanLocal bucketBean;
    
    @EJB
    private LoggerSessionBeanLocal loggerBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String param = request.getParameter("id");
        if (param == null || param.trim().isEmpty()) {
            request.setAttribute("goods", bucketBean.getAllGoods());
            request.setAttribute("quantity", bucketBean.getQuantityOfGoodsInBucket());
            request.setAttribute("total", bucketBean.getTotalPriceOfAllProducts());
            request.setAttribute("last", bucketBean.getLastAddedGood());
            request.getRequestDispatcher("/WEB-INF/views/bucket.jsp").forward(request, response);
        } else {
            int id = Integer.parseInt(param);
            Good good = goodsBean.get(id);
            bucketBean.addGoodIntoBucket(good);
            // log good and user info
            loggerBean.writeLog(good, request.getSession().getId());
            response.sendRedirect("bucket");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
