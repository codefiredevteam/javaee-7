/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.web.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ua.com.codefire.beans.sessions.stateless.GoodsSessionBeanLocal;

/**
 *
 * @author codefire
 */
@WebServlet("/all")
public class ListController extends HttpServlet{
    
    @EJB
    private GoodsSessionBeanLocal goodsBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("goods", goodsBean.getAll());
        request.getRequestDispatcher("WEB-INF/views/list.jsp").forward(request, response);
    }
    
}
