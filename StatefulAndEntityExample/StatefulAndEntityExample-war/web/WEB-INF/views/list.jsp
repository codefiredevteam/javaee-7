<%-- 
    Document   : list
    Created on : Jul 28, 2016, 12:40:04 PM
    Author     : codefire
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>List of Goods</h3>
        <table border="1">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="good" items="${goods}">
                    <tr>
                        <td>${good.name}</td>
                        <td>${good.price}</td>
                        <td><a href="bucket?id=${good.id}">Add to bucket</a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table><br />
        <a href="index.html">Back</a>
    </body>
</html>
