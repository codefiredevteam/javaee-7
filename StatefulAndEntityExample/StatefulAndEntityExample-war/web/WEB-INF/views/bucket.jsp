<%-- 
    Document   : bucket
    Created on : Jul 28, 2016, 2:48:19 PM
    Author     : codefire
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>Your Bucket</h3>
        <c:choose>
            <c:when test="${not empty last}">
                <h4>Last added: ${last.name}</h4>
            </c:when>
            <c:otherwise>
                <h4>Has no goods!</h4>
            </c:otherwise>
        </c:choose>
        <table border="1">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${goods}">
                    <tr>
                        <td>${item.key.name}</td>
                        <td>${item.value}</td>
                        <td>$ <fmt:formatNumber maxFractionDigits="2" value="${item.key.price * item.value}" /></td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="2">Quantity</td>
                    <td>${quantity}</td>
                </tr>
                <tr>
                    <td colspan="2">Total</td>
                    <td>$ ${total}</td>
                </tr>
            </tbody>
        </table><br />
        <a href="all">Add more goods</a>
    </body>
</html>
